---
marp: true
paginate: true
---

# **Free Software Rug Pull**

How to Keep Your Balance

- Michael Shoup
- m@shoup.io

Source code: https://gitlab.com/shouptech/rug-pull-presentation

License [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)

---

# Some terms

* **Free Software / Open Source**: Software in which you have been granted the ["Four Freedoms"][1]. *Open source* and *free software* are functionally synonymous. [The differences are not important to this discussion][2].
* **Source available**: Software that is not *open source* or *free software* but still provides source code. These licenses greatly restrict some or all of the freedoms. *Source available* licenses are also considered nonfree.
* **Copyright**: A right granted to the creator of a work or another right holder that allows them to control their creation.
* **Contributor License Agreement**: Common among free software projects ran by corporations, an agreement that you must sign before contributing code to a project. Typically grants a copyright license, or reassigns copyright to the corporation.


[1]: https://www.gnu.org/philosophy/free-sw.en.html#four-freedoms
[2]: https://www.gnu.org/philosophy/open-source-misses-the-point.en.html

---

# Rug pull example

* Elastic releases Elasticsearch and Kibana under the Apache License, v2.0. This makes it *free software*.
* Elastic builds a community around Elasticsearch, accepting contributions from the community in exchange for signing a *CLA*.
* Elastic sells support services for Elasticsearch, as well as "Elasticsearch Cloud", their own PaaS offering of Elasticsearch.
* AWS simultaneously offers Elasticsearch Service, based on the *free* versions of Elasticsearch.
* Elastic and AWS have a row.
* Elastic drops the APLv2 for both the Elastic License and the SSPL.
* AWS forks Elasticsearch and creates OpenSearch.

---

# Is that legal?

* Copyright holders can relicense their own works.
* Contributors signed a *contributor license agreement*!
* Example from [HashiCorp's CLA][3]:
  > Grant of Copyright License. Subject to the terms and conditions of this Agreement, You hereby grant to HashiCorp and to recipients of software distributed by HashiCorp a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare derivative works of, publicly display, publicly perform, sublicense, and distribute Your Contributions and such derivative works.
* In human terms: HashiCorp can do whatever they want with your code.
* **Important**: Software released under a previous license doesn't go away.

[3]: https://www.HashiCorp.com/cla

---

# Infamous rug pulls

* **MongoDB**: AGPL3 → Server Side Public License in 2018 (Created the SSPL)
* **Elasticsearch and Kibana**: Apache 2.0 → Elastic License / SSPL in 2021
* **HashiCorp**: Mozilla Public License → Business Source License for virtually all software in 2023
* **Redis Core**: BSD 3-clause → Redis Source Available License / SSPL in 2024 (Modules relicensed in 2018 and again in 2022)

---

# But What About Red Hat?

* Red Hat used to provide source code to everything in RHEL to the public for *free*.
* Now you have to have a software subscription to download the source code.
* Is that considered a rug pull?
* **Yes**, *but*...
* It's a different kind of rug pull. Free software licenses only require distribution of the source code with the software itself. 
* The GPL and other licenses don't disallow paywalls.
* Red Hat also claims it will cancel a user's subscription for distributing source code they downloaded. Feel free to challenge IBM on the legality of this.

---

# Can rug pulls be prevented?

* Nope.
* **Really?**
* Don't use free software controlled by organizations you don't trust.
  * Not necessarily practical.
  * Prefer software controlled by foundations that have a history of being good stewards.
  * Linux Foundation, Apache Software Foundation, GNU, etc.
* Don't sign a CLA in order to be a contributor.
  * But then would you be a contributor? 🤔

---

# Should we care?

* It's the principle of it!
* Also, ask your lawyers.

---

# Community response

* Fork it!
  * Elasticsearch / Kibana → [OpenSearch](https://opensearch.org/)
  * HashiCorp Terraform → [OpenTofu](https://opentofu.org/)
  * HashiCorp Vault → [OpenBao](https://openbao.org/)
  * Redis → [Valkey](https://valkey.io/)

* Use alternatives
  * MongoDB → [FerretDB](https://www.ferretdb.com/)
  * Redis → [KeyDB](https://docs.keydb.dev/)

---

# Etc.

- **List of free software licenses**: https://www.gnu.org/licenses/license-list.html
- **What is free software? GNU.org**: https://www.gnu.org/philosophy/free-sw.en.html
- **Corporate Open Source is Dead, Jeff Geerling**: https://www.jeffgeerling.com/blog/2024/corporate-open-source-dead
- **Community perspectives on Elastic vs AWS**: https://changelog.com/podcast/429
- **The SSPL is Not an Open Source License**: https://opensource.org/blog/the-sspl-is-not-an-open-source-license