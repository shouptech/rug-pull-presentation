# The Rug Pull Presentation

This project uses [Marp](https://marp.app/) to generate slides from a markdown file.

## View the presentation

View the presentation at: https://shouptech.gitlab.io/rug-pull-presentation/

## Modifying

Edit the file `presentation.md`

## Generate slides

First, install the Marp CLI. Then:

```shell
# Generate HTML
marp presentation.md -o out/presentation.html

# Generate PDF
marp presentation.md -o out/presentation.pdf

# Generate PPTX
marp presentation.md -o out/presentation.pptx
```

## License

 This work © 2024 by Michael Shoup is licensed under CC BY 4.0. To view a copy of this license, visit https://creativecommons.org/licenses/by/4.0/